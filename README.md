Cross-compilation toolchains for TVSC

!! Building the Raspberry Pi 3 cross-compiler
# Use crosstools-ng: https://github.com/crosstool-ng/crosstool-ng
# Note: installed on my local computer (JP) under
# export CT_BIN=~/tvsc/crosstool-ng/bin
#
# Initialize the configuration in .config. (Can also cp from a previous configuration: cp aarch64-rpi3-linux-gnu.config .config)
## ${CT_BIN}/ct-ng aarch64-rpi3-linux-gnu
# Modify the configuration
## ${CT_BIN}/ct-ng menuconfig
## In menuconfig, make these changes:
## Change C library version to 2.31, the version on Raspberry Pi OS
## Add strace support
## Select gcc-10, since gcc-11 has an issue with pthread requiring an array for struct __jmp_buf_tag but receiving a pointer.
## Set oldest supported ABI to GLIBC_2.30
# Make the full config file available in this repo (aarch64-rpi3-linux-gnu.config)
# cp .config aarch64-rpi3-linux-gnu.config
# ${CT_BIN}/ct-ng build
# rm -rf aarch64-rpi3-linux-gnu; (cd ~/x-tools/; tar cf - aarch64-rpi3-linux-gnu/) | tar xf -; git add aarch64-rpi3-linux-gnu
